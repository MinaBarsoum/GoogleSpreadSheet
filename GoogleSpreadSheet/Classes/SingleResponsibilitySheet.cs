﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;
using GoogleSpreadSheet.DBLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;


namespace GoogleSpreadSheet.Classes
{
    public class SingleResponsibilitySheet 
    {

        public string Asset { get; set; }
        public string AssetName { get; set; }
        public string Model { get; set; }

        public string Vendor { get; set; }
        public string Description { get; set; }


        public IList<IList<Object>>  ImportData()
        {
          
            
            string[] Scopes = { SheetsService.Scope.Spreadsheets };
            var credential = GoogleCredential.FromStream(new FileStream(new System.Web.UI.Page().Server.MapPath("~/Helpers/solar-curve-326618-a9615b06b97b.json"), FileMode.Open)).CreateScoped(Scopes);
            var service = new SheetsService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = "ApplicationName",
            });

            // Define request parameters.  
            String spreadsheetId = "1ey9bJZCsyHxMjPfRL-8yiuaDwrfgpMsfXbUJ1unkaY4";
            String range = "Sheet1";
            SpreadsheetsResource.ValuesResource.GetRequest request =
                                    service.Spreadsheets.Values.Get(spreadsheetId, range);
            // Prints the names and majors of students in a sample spreadsheet:  
            ValueRange response = request.Execute();
            return response.Values;
            //IList<IList<Object>> values = response.Values;

        }

        public void ClearData()
        {


            DB_Sheet.ClearSheet();

        }
        public void SaveData(IList<IList<Object>> values)
        {


            DB_Sheet.SaveSheet(values);

        }
        public List<SingleResponsibilitySheet> GetDataOnLine(IList<IList<Object>> values)
        {
            List<SingleResponsibilitySheet> LstSheet = new List<SingleResponsibilitySheet>();

            for (int i = 0; i < values.Count; i++)
            {
                if (values[i][0].ToString().ToLower() == "asset #")
                    continue;
                SingleResponsibilitySheet Sheet = new SingleResponsibilitySheet();
                Sheet.Asset = values[i][0].ToString();
                Sheet.AssetName = values[i][1].ToString();
                Sheet.Model = values[i][2].ToString();
                Sheet.Vendor = values[i][3].ToString();
                Sheet.Description = values[i][4].ToString();
                LstSheet.Add(Sheet);
            }
            return LstSheet;

        }
        public List<SingleResponsibilitySheet> GetData()
        {
            List<SingleResponsibilitySheet> LstSheet = new List<SingleResponsibilitySheet>();
            
            DataSet DsSheet= DB_Sheet.GetSheet();
            for (int i = 0; i < DsSheet.Tables[0].Rows.Count; i++)
            {
                SingleResponsibilitySheet Sheet = new SingleResponsibilitySheet();
                Sheet.Asset = DsSheet.Tables[0].Rows[i][1].ToString();
                Sheet.AssetName = DsSheet.Tables[0].Rows[i][2].ToString();
                Sheet.Model = DsSheet.Tables[0].Rows[i][3].ToString();
                Sheet.Vendor = DsSheet.Tables[0].Rows[i][4].ToString();
                Sheet.Description = DsSheet.Tables[0].Rows[i][5].ToString();
                LstSheet.Add(Sheet);
            }
            return LstSheet;

        }
    }
}