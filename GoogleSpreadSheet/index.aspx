﻿
<!DOCTYPE html>

<html>
<head>
    <title>GoogleSpreadSheet</title>

    <link href="Content/kendo/2023.1.117/kendo.default-main.min.css" rel="stylesheet" />
    <script src="Scripts/jquery-3.4.1.min.js"></script>
    <script src="Scripts/kendo/2023.1.117/kendo.all.min.js"></script>
    <style type="text/css">
        .auto-style1 {
            width: 108px
        }
        .auto-style2 {
            width: 284px;
        }
    </style>
</head>
<body>
     <form id="form1" runat="server">
    
         <table style="width:100%;">
             <tr>
                 <td class="auto-style1">
                     <asp:Label ID="lblShowType" runat="server" Text="Show Type"></asp:Label>
                 </td>
                 <td class="auto-style2"> 
                     <asp:DropDownList ID="ddlShowType" runat="server">
            <asp:ListItem Value="0">FromGoogleDrive</asp:ListItem>
            <asp:ListItem Value="1">FromDB</asp:ListItem>
        </asp:DropDownList>&nbsp;</td>
                 <td>
                     <input id="btnShow" type="button" value="Show" onclick="ShowData(); return false;" /></td>
             </tr>
            
            
         </table>
      <div id="sheetsGrid"></div>&nbsp;

    <script>
        function ShowData() {

    $("#sheetsGrid").kendoGrid({
        columns: [
            { field: "Asset", title: "Asset #" },
            { field: "AssetName", title: "Asset Name" },
            { field: "Model", title: "Model #" },
            { field: "Vendor", title: "Vendor" },
            { field: "Description", title: "Description" },
        ],
        dataSource: new kendo.data.DataSource({
            transport: {
                read: "api/sheets?ShowType=" + $("#ddlShowType").val()
            },
            pageSize: 3
        }),
        sortable: true,
        pageable: true
    });
   
};
    </script>


     </form>


</body>
</html>
