﻿using GoogleSpreadSheet.Classes;
using GoogleSpreadSheet.DBLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace GoogleSpreadSheet.Controllers
{
    public class SheetsController:ApiController
    {
        public List<SingleResponsibilitySheet> Get(int ShowType)
        {
            SingleResponsibilitySheet singleResponsibilitySheet = new SingleResponsibilitySheet();
           
            if (ShowType == 0)
            {
                IList<IList<Object>> values = singleResponsibilitySheet.ImportData();
                singleResponsibilitySheet.ClearData();
                singleResponsibilitySheet.SaveData(values);
                return singleResponsibilitySheet.GetDataOnLine(values);
            }
            else
            {
                return singleResponsibilitySheet.GetData();
            }




        }
      
    }
}