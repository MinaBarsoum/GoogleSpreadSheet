﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DB = Microsoft.ApplicationBlocks.Data.SqlHelper;


namespace GoogleSpreadSheet.DBLayer
{
    public class DB_Sheet
    {

        public static DataSet GetSheet()
        {
            
            return DB.ExecuteDataset(Connection.DB_Connect(), "GetSheet");


            
        }

        public static void ClearSheet()
        {

            DB.ExecuteNonQuery(Connection.DB_Connect(), "DeleteSheet");



        }
        public static  void SaveSheet(IList<IList<Object>> values)
        {

            DataTable sheet = new DataTable();
            sheet.Columns.Add(new DataColumn("Asset", typeof(string)));
            sheet.Columns.Add(new DataColumn("AssetName", typeof(string)));
            sheet.Columns.Add(new DataColumn("Model", typeof(string)));
            sheet.Columns.Add(new DataColumn("Vendor", typeof(string)));
            sheet.Columns.Add(new DataColumn("Description", typeof(string)));

            for (int i = 0; i < values.Count; i++)
            {

                DataRow dr = sheet.NewRow();
                dr["Asset"] = values[i][0];
                dr["AssetName"] = values[i][1];
                dr["Model"] = values[i][2];
                dr["Vendor"] = values[i][3];
                dr["Description"] = values[i][4];
                sheet.Rows.Add(dr);
            }

           
            //create object of SqlBulkCopy which help to insert  
            SqlBulkCopy objbulk = new SqlBulkCopy(Connection.SQLConnect());

            //assign Destination table name  
            objbulk.DestinationTableName = "Sheet";
            objbulk.ColumnMappings.Add("Asset", "Asset");
            objbulk.ColumnMappings.Add("AssetName", "AssetName");
            objbulk.ColumnMappings.Add("Model", "Model");
            objbulk.ColumnMappings.Add("Vendor", "Vendor");
            objbulk.ColumnMappings.Add("Description", "Description");
            if(Connection.SQLConnect().State==ConnectionState.Closed)
            Connection.SQLConnect().Open();
            //insert bulk Records into DataBase.  
            objbulk.WriteToServer(sheet);
            Connection.SQLConnect().Close();

        }


    }
}