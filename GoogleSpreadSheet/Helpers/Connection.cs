﻿
using System.Data.SqlClient;

namespace GoogleSpreadSheet
{
    public static class Connection
    {
       private  static string MainConnection;
  

        public static string DB_Connect()
        {
          return  MainConnection = System.Configuration.ConfigurationManager.ConnectionStrings["GoogleSpreadSheetConnection"].ConnectionString;
        }
     

        public static SqlConnection SQLConnect()
        {
            SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["GoogleSpreadSheetConnection"].ConnectionString);
            con.Open();
            return con;
        }

    }
}